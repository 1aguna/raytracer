// inline Float Lerp(Float t, Float v1, Float v2) { return (1 - t) * v1 + t * v2; }

pub fn lerp(t: f32, v1: f32, v2: f32) -> f32 {
    (1.0 - t) * v1 + t * v2
}