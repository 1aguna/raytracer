use crate::{
    geometry::point::{Point3},
    core::{lerp},
};

#[derive(Debug, Copy, Clone)]
pub struct Bounds3 {
    pub p_min: Point3,
    pub p_max: Point3,
}

impl Bounds3 {
    pub fn lerp(&self, p: &Point3) -> Point3 {
        /*
         Point3f {
            x: lerp(t.x, self.p_min.x as Float, self.p_max.x as Float),
            y: lerp(t.y, self.p_min.y as Float, self.p_max.y as Float),
            z: lerp(t.z, self.p_min.z as Float, self.p_max.z as Float),
        }
         */
        !unimplemented!()
    }
}

