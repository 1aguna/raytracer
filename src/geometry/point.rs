use std::ops::{Add, Sub, Mul, Div, Neg, Index, IndexMut};
use crate::geometry::vector::Vector3;
/// Point3 is a standard 3 component Point3 but transforms as a Point3
/// Point3 when transformations are applied
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Point3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Point3 {
    /// Initialize the Point3 and set values for x, y, z
    pub fn new(x: f32, y: f32, z: f32) -> Point3 {
        Point3 { x, y, z }
    }
    /// Initialize the Point3 with the same value of x, y, z
    pub fn broadcast(x: f32) -> Point3 {
        Point3 { x: x, y: x, z: x }
    }
    /// Initialize a Point3 to be all 0 values
    pub fn origin() -> Point3 {
        Point3::broadcast(0.0)
    }
    /// Compute the squared distance between this Point3 and another
    pub fn distance_squared(&self, a: &Point3) -> f32 {
        (*self - *a).length_squared()
    }
    /// Compute the distance between this Point3 and another
    pub fn distance(&self, a: &Point3) -> f32 {
        (*self - *a).length()
    }
}

impl Add for Point3 {
    type Output = Point3;
    /// Add two Point3s together
    fn add(self, rhs: Point3) -> Point3 {
        Point3 { x: self.x + rhs.x, y: self.y + rhs.y, z: self.z + rhs.z }
    }
}

impl Add<Vector3> for Point3 {
    type Output = Point3;
    /// Add two Point3s together
    fn add(self, rhs: Vector3) -> Point3 {
        Point3 { x: self.x + rhs.x, y: self.y + rhs.y, z: self.z + rhs.z }
    }
}

impl Sub for Point3 {
    type Output = Vector3;
    /// Subtract two Point3s to get the Vector3 between them
    fn sub(self, rhs: Point3) -> Vector3 {
        Vector3 { x: self.x - rhs.x, y: self.y - rhs.y, z: self.z - rhs.z }
    }
}

impl Sub<Vector3> for Point3 {
    type Output = Point3;
    /// Subtract a Vector3 from a Point3, translating the Point3 by -Vector3
    fn sub(self, rhs: Vector3) -> Point3 {
        Point3 { x: self.x - rhs.x, y: self.y - rhs.y, z: self.z - rhs.z }
    }
}

impl Mul<f32> for Point3 {
    type Output = Point3;
    /// Scale the Point3 by some value
    fn mul(self, rhs: f32) -> Point3 {
        Point3 { x: self.x * rhs, y: self.y * rhs, z: self.z * rhs }
    }
}

impl Mul<Point3> for f32 {
    type Output = Point3;
    /// Scale the Vector3 by some value
    fn mul(self, rhs: Point3) -> Point3 {
        Point3 { x: self * rhs.x, y: self * rhs.y, z: self * rhs.z }
    }
}

impl Mul<Vector3> for Point3 {
    type Output = Point3;
    /// Scale the Vector3 by some value
    fn mul(self, rhs: Vector3) -> Point3 {
        Point3 { x: self.x * rhs.x, y: self.y * rhs.y, z: self.z * rhs.z }
    }
}

impl Div for Point3 {
    type Output = Point3;
    /// Divide the Point3s components by the right hand side's components
    fn div(self, rhs: Point3) -> Point3 {
        Point3 { x: self.x / rhs.x, y: self.y / rhs.y, z: self.z / rhs.z }
    }
}

impl Div<f32> for Point3 {
    type Output = Point3;
    /// Divide the Point3s components by scalar
    fn div(self, rhs: f32) -> Point3 {
        Point3 { x: self.x / rhs, y: self.y / rhs, z: self.z / rhs }
    }
}

impl Neg for Point3 {
    type Output = Point3;
    /// Negate the Point3
    fn neg(self) -> Point3 {
        Point3 { x: -self.x, y: -self.y, z: -self.z }
    }
}

impl Index<usize> for Point3 {
    type Output = f32;
    /// Access the Point3 by index
    ///
    /// - 0 = x
    /// - 1 = y
    /// - 2 = z
    fn index(&self, i: usize) -> &f32 {
        match i {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Invalid index into Point3"),
        }
    }
}

impl IndexMut<usize> for Point3 {
    /// Access the Point3 by index
    ///
    /// - 0 = x
    /// - 1 = y
    /// - 2 = z
    fn index_mut(&mut self, i: usize) -> &mut f32 {
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            _ => panic!("Invalid index into Point3"),
        }
    }
}