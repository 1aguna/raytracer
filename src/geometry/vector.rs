use std::ops::{Add, Sub, Mul, Div, Neg, Index, IndexMut, AddAssign, MulAssign, DivAssign};
use std::ops;

use crate::geometry::point::Point3;

/// Vector3 is a standard 3 component vector
#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    /// Initialize the vector and set values for x, y, z
    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x: x, y: y, z: z }
    }
    /// Initialize the vector with the same value of x, y, z
    pub fn broadcast(x: f32) -> Vector3 {
        Vector3 { x: x, y: x, z: x }
    }
    /// Compute the squared length of the vector
    pub fn length_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
    /// Compute the length of the vector
    pub fn length(&self) -> f32 {
        self.length_squared().sqrt()
    }
    /// Get a normalized copy of this vector
    pub fn normalized(&self) -> Vector3 {
        let len = self.length();
        Vector3 { x: self.x / len, y: self.y / len, z: self.z / len }
    }
    /// Normalize the vector in-place
    pub fn normalize(&mut self)  {
        let len = self.length();
        *self /= len;
    }

    pub fn dot(&self, b: &Vector3) -> f32 {
        self.x * b.x +
        self.y * b.y +
        self.z * b.z
    }

    pub fn cross(&self, b: &Vector3) -> Vector3 {
        Vector3::new(
            self.y * b.z - self.z * b.y,
            self.z * b.x - self.x * b.z,
            self.x * b.y - self.y * b.x,
        )
    }

    pub fn has_nans(&self) -> bool {
        self.x.is_nan() || self.y.is_nan() || self.z.is_nan()
    }
}

// Binary Operators
impl_op_ex!(+ |a: &Vector3, b: &Vector3| -> Vector3 {
    Vector3 {
        x: a.x + b.x,
        y: a.y + b.y,
        z: a.z + b.z
    }
});

impl_op_ex!(- |a: &Vector3, b: &Vector3| -> Vector3 {
    Vector3 {
        x: a.x - b.x,
        y: a.y - b.y,
        z: a.z - b.z
    }
});

impl_op_ex_commutative!(* |a: &Vector3, scalar: f32 | -> Vector3 {
    Vector3 {
        x: a.x * scalar,
        y: a.y * scalar,
        z: a.z * scalar,
    }
});

impl_op_ex!(/ |a: &Vector3, scalar: f32| -> Vector3 {
    Vector3 {
        x: a.x / scalar,
        y: a.y / scalar,
        z: a.z / scalar,
    }
});

// Unary operators
impl_op_ex!(- |a: &Vector3| -> Vector3 {
    Vector3 {
        x: -a.x,
        y: -a.y,
        z: -a.z,
    }
});

// Assignment operators
impl_op_ex!(+= |a: &mut Vector3, b: &Vector3| {
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
});


impl_op_ex!(-= |a: &mut Vector3, b: &Vector3| {
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
});

impl_op!(/= |a: &mut Vector3, b: f32| {
    a.x /= b;
    a.y /= b;
    a.z /= b;
});

impl_op_ex!(*= |a: &mut Vector3, b: f32| {
    a.x *= b;
    a.y *= b;
    a.z *= b;
});


impl Index<usize> for Vector3 {
    type Output = f32;
    /// Access the vector by index
    ///
    /// - 0 = x
    /// - 1 = y
    /// - 2 = z
    fn index(&self, i: usize) -> &f32 {
        match i {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Invalid index into vector"),
        }
    }
}

impl IndexMut<usize> for Vector3 {
    /// Access the vector by index
    ///
    /// - 0 = x
    /// - 1 = y
    /// - 2 = z
    fn index_mut(&mut self, i: usize) -> &mut f32 {
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            _ => panic!("Invalid index into vector"),
        }
    }
}

pub fn vec3_dot_vec3(a: &Vector3, b: &Vector3) -> f32 {
    a.dot(b)
}

pub fn vec3_cross_vec3(a: &Vector3, b: &Vector3) -> Vector3 {
    a.cross(b)
}

/// Construct a local coordinate system given only a single 3D vector.
pub fn vec3_coordinate_system(v1: &Vector3, v2: &mut Vector3, v3: &mut Vector3) {
    if v1.x.abs() > v1.y.abs() {
        *v2 = Vector3 {
            x: -v1.z,
            y: 0.0,
            z: v1.x,
        } / (v1.x * v1.x + v1.z * v1.z).sqrt();
    } else {
        *v2 = Vector3 {
            x: 0.0,
            y: v1.z,
            z: -v1.y,
        } / (v1.y * v1.y + v1.z * v1.z).sqrt();
    }
    *v3 = vec3_cross_vec3(v1, &*v2);
}

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn new(x: f32, y: f32) -> Vector2 {
        Vector2 { x: x, y: y}
    }

    pub fn length_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }

    pub fn length(&self) -> f32 {
        self.length_squared().sqrt()
    }

    pub fn has_nans(&self) -> bool {
        self.x.is_nan() || self.y.is_nan()
    }

    pub fn dot(&self, b: &Vector3) -> f32 {
        self.x * b.x +
        self.y * b.y 
    }
}

// impl_op!(-|a: Vector2| -> Vector2 { Vector2 { x: -a.x, y: -a.y } });


// Binary Operators
impl_op_ex!(+ |a: &Vector2, b: &Vector2| -> Vector2 {
    Vector2 {
        x: a.x + b.x,
        y: a.y + b.y    }
});

impl_op_ex!(- |a: &Vector2, b: &Vector2| -> Vector2 {
    Vector2 {
        x: a.x - b.x,
        y: a.y - b.y,
    }
});

impl_op_ex_commutative!(* |a: &Vector2, scalar: f32 | -> Vector2 {
    Vector2 {
        x: a.x * scalar,
        y: a.y * scalar,
    }
});

impl_op_ex!(/ |a: &Vector2, scalar: f32| -> Vector2 {
    Vector2 {
        x: a.x / scalar,
        y: a.y / scalar,
    }
});

// Unary operators
impl_op_ex!(- |a: &Vector2| -> Vector2 {
    Vector2 {
        x: -a.x,
        y: -a.y,
    }
});

// Assignment operators
impl_op_ex!(+= |a: &mut Vector2, b: &Vector2| {
    a.x += b.x;
    a.y += b.y;
});


impl_op_ex!(-= |a: &mut Vector2, b: &Vector2| {
    a.x -= b.x;
    a.y -= b.y;
});

impl_op!(/= |a: &mut Vector2, b: f32| {
    a.x /= b;
    a.y /= b;
});

impl_op_ex!(*= |a: &mut Vector2, b: f32| {
    a.x *= b;
    a.y *= b;
});


impl Index<usize> for Vector2 {
    type Output = f32;
    /// Access the vector by index
    ///
    /// - 0 = x
    /// - 1 = y
    fn index(&self, i: usize) -> &f32 {
        match i {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("Invalid index into vector"),
        }
    }
}

impl IndexMut<usize> for Vector2 {
    /// Access the vector by index
    ///
    /// - 0 = x
    /// - 1 = y
    fn index_mut(&mut self, i: usize) -> &mut f32 {
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            _ => panic!("Invalid index into vector"),
        }
    }
}